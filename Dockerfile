# Copyright (c) 2019-present Sonatype, Inc. All rights reserved.
# Includes the third-party code listed at http://links.sonatype.com/products/nexus/attributions.
# "Sonatype" is a trademark of Sonatype, Inc.
ARG REPOSITORY
ARG IQ_CLI_IMAGE_VERSION
FROM koraytugay/nexuscli:latest

COPY target/gitlab-nexus-platform-plugin-jar-with-dependencies.jar ${SONATYPE_LIB}/gitlab-nexus-platform-plugin.jar
RUN mv ${SONATYPE_BIN}/evaluate ${SONATYPE_BIN}/iqCliPolicyEval
COPY src/main/sh/evaluate ${SONATYPE_BIN}/evaluate

RUN find ${SONATYPE_BIN} -type f -exec chmod +x {} \;
