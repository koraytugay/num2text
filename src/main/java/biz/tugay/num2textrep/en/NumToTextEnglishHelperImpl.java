package biz.tugay.num2textrep.en;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

class NumToTextEnglishHelperImpl implements NumToTextEnglishHelper {

    private static final ResourceBundle englishMappings = ResourceBundle.getBundle("mappings_en");

    @Override
    public List<String> partitionInThreeDigits(String aString) {
        final List<String> divided = new ArrayList<>();
        for (int i = aString.length(); i > 0; i = i - 3) {
            divided.add(aString.substring(Math.max(0, i - 3), i));
        }
        return divided;
    }

    @Override
    public String resolveThreeDigitNumberConsideringLevel(String input, int level) {
        if (input.length() > 3) {
            throw new UnsupportedOperationException("Accepted values are strings with at most 3 digits!");
        }

        // Level 0 is where we do not have thousand or millions or anything..
        // If we have zero, just return 0..
        if (level == 0 && input.equals("0")) {
            return englishMappings.getString(input);
        }

        // We should not be doing anything if we simply have only zeros, regardless of any level we are at..
        if (input.equals("00") || input.equals("000")) {
            return "";
        }

        // Delegate resolving string representation of digits we have to helper methods..
        String resolved = "";
        switch (input.length()) {
            case 1:
                resolved = englishMappings.getString(input);
                break;
            case 2:
                resolved = resolveTwoDigit(input, resolved);
                break;
            case 3:
                resolved = resolveThreeDigitAtLevel(input, level, resolved);
                break;
        }

        // If we are at any level above zero, we need to append the required string representation
        resolved = appendScaleInfo(level, resolved);

        return resolved.trim();
    }

    private String resolveTwoDigit(String triplet, String resolved) {
        // If we have an exact match, simply return that.
        // These are eleven, twelve etc..
        // Values that do not follow rules
        if (englishMappings.containsKey(triplet)) {
            resolved = resolved + englishMappings.getString(triplet);
        } else {
            // Otherwise match the tens as in: thirty, forty etc by using the first digit appended by a zeor
            resolved = englishMappings.getString(triplet.substring(0, 1) + "0");
            // Resolve the last digit
            resolved = resolved + " " + englishMappings.getString(triplet.substring(1, 2));
        }
        return resolved;
    }

    private String resolveThreeDigitAtLevel(String input, int level, String resolved) {
        if (input.length() != 3) {
            throw new UnsupportedOperationException("Resolves only 3 digit inputs!");
        }
        String substring = input.substring(0, 1);
        if (!substring.equals("0")) {                           // If first digit is not zero, we have a hundred..
            resolved = englishMappings.getString(substring);    // This is where we handle it
            resolved = resolved + " hundred";
        }
        if (!input.substring(1, 3).equals("00")) {              // If last 2 digits are not both zero we need to resolve the last 2 digits as well
            if (level == 0) {                                   // This is a special case, if we are at level 0 (in other word tens or hundreds),
                                                                // we want to say (one, five, etc) .. hundred AND .. (eleven, fifty etc..)
                resolved = resolved + " and";
            }
            String zerosRemoved = StringUtils.stripStart(input.substring(1, 3), "0");
            if (englishMappings.containsKey(zerosRemoved)) {    // This is pretty much same with resolving a 2 digit input
                resolved = resolved + " " + englishMappings.getString(zerosRemoved);
            } else {
                resolved = resolved + " " + englishMappings.getString(input.substring(1, 2) + "0");
                resolved = resolved + " " + englishMappings.getString(input.substring(2, 3));
            }
        }
        return resolved;
    }

    private String appendScaleInfo(int level, String resolved) {
        switch (level) {
            case 0:
                break;
            case 1:
                resolved = resolved + " thousand";
                break;
            case 2:
                resolved = resolved + " million";
                break;
            case 3:
                resolved = resolved + " billion";
                break;
            case 4:
                resolved = resolved + " trillion";
                break;
            case 5:
                resolved = resolved + " quadrillion";
                break;
            case 6:
                resolved = resolved + " quintillion";
                break;
            case 7:
                resolved = resolved + " sextillion";
                break;
            default:
                resolved = resolved + " ¯\\_(ツ)_/¯";
                break;
        }
        return resolved;
    }
}
