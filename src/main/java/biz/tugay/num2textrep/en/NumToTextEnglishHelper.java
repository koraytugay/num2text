package biz.tugay.num2textrep.en;

import java.util.List;

public interface NumToTextEnglishHelper {
    /**
     * Examples:
     * "1" -> ["1"]
     * "12" -> ["12"]
     * "123" -> ["123"]
     * "1234" -> ["123", "4"]
     * "12345" -> ["123", "45"]
     * @param aString Partitions a given string starting from end moving to index 0.
     * @return A List where each element contains at most 3 characters.
     */
    List<String> partitionInThreeDigits(String aString);


    /**
     * @param input A 3 digit input
     * @param level The level this 3 digit input must be resolved against.
     *              Definition of level is as follows:
     *              For a given number: xxx,yyy,zzz
     *              Levels would be:     2   1   0
     *              Levels are roughly tens, thousands, millions etc..
     * @return String representation of the input
     */
    String resolveThreeDigitNumberConsideringLevel(String input, int level);
}
