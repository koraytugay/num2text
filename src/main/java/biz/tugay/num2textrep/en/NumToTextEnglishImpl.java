package biz.tugay.num2textrep.en;

import biz.tugay.num2textrep.NumToText;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class NumToTextEnglishImpl implements NumToText {

    private NumToTextEnglishHelper numToTextEnglishHelper = new NumToTextEnglishHelperImpl();

    @Override
    public String wholeNumberToText(String aWholeNumber) {
        // Clean any leading or trailing whitespace
        String sanitized = aWholeNumber.trim();

        // Figure out if the provided input represents a negative number
        boolean isNegative = false;
        if (sanitized.startsWith("-")) {
            sanitized = sanitized.substring(1).trim();  // We are not interested in the minus sign after this point
            if (!sanitized.equals("0")) {               // This is where we handle a potential "Minus Zero" which would not make sense
                isNegative = true;
            }
        }

        if (!StringUtils.isNumeric(sanitized)) {        // The input at this point must be consist of only digits
            throw new NumberFormatException();
        }

        // Make sure the provided value is within acceptable limits
        if (getLowerBound() != null && Long.parseLong(aWholeNumber) < getLowerBound()) {
            throw new IllegalArgumentException("Value too small!");
        }
        if (getUpperBound() != null && Long.parseLong(aWholeNumber) > getUpperBound()) {
            throw new IllegalArgumentException("Value too large!");
        }

        if (sanitized.length() > 1 && sanitized.startsWith("0")) {  // 0 is fine but what is 01? Leading 0s are not acceptable
            throw new NumberFormatException();
        }

        // This is where we finally process the input
        List<String> partitioned = numToTextEnglishHelper.partitionInThreeDigits(sanitized);
        StringBuilder result = new StringBuilder();
        for (int i = partitioned.size() - 1; i >= 0; i--) {
            String str = numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel(partitioned.get(i), i);
            if (!StringUtils.isBlank(str)) {
                result.append(str).append(" ");
            }
        }

        // Capitalize, append Minus if needed and return
        String formatted = StringUtils.capitalize(result.toString().trim());
        return isNegative ? "Minus " + formatted : formatted;
    }
}
