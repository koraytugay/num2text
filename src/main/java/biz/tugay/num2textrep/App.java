package biz.tugay.num2textrep;

import biz.tugay.num2textrep.en.NumToTextEnglishImpl;

import java.util.Scanner;

public class App {
    private final NumToText numToText = new NumToTextEnglishImpl();

    // Application wide configuration passed through runtime parameters
    // TODO: Perhaps use a proper logging framework..
    private static boolean stackTraceSuppressed = false;

    public static void main(String[] args) {
        new App().run(args);
    }

    private void run(String[] args) {
        if ("true".equals(System.getProperty("supressStackTraces", "false"))) {
            stackTraceSuppressed = true;
        }
        System.out.println("Excepted values range: [" + numToText.getLowerBound() + "," + numToText.getUpperBound() + "]");
        if (args.length > 0) {
            runBatchMode(args);
        } else {
            runInteractiveMode();
        }
    }

    private void runBatchMode(String[] args) {
        for (String argument : args) {
            System.out.println(argument + " : " + resolveInput(argument));
        }
    }

    private void runInteractiveMode() {
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter a whole number or 'q' to quit.");
            String input = scanner.nextLine();
            if (input.equals("q")) {
                break;
            }
            System.out.println(resolveInput(input));
        }
        scanner.close();
    }

    private String resolveInput(String input) {
        try {
            return numToText.wholeNumberToText(input);
        } catch (IllegalArgumentException e) {
            if (!stackTraceSuppressed) {
                e.printStackTrace();
            }
            return "Illegal Argument!";
        }
    }
}