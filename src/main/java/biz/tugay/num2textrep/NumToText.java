package biz.tugay.num2textrep;

import org.apache.commons.lang3.StringUtils;

import java.util.ResourceBundle;

public interface NumToText {

    ResourceBundle config = ResourceBundle.getBundle("config");

    /**
     * @param aWholeNumber A String that represents a number.
     *                     Example valid values: "1", "-1", "- 1", "12341", "0"..
     * @return Returns the String representation. "One", "Minus One", "Zero", "One hundred and then"..
     * @throws NumberFormatException In case the passed argument does not represent a number
     */
    String wholeNumberToText(String aWholeNumber) throws NumberFormatException;

    /**
     * Reads lowerbound from config file.
     * Can be overridden by implementations for different calculation strategies.
     * It is the implementations responsibility to make sure bounds are checked.
     * @return The lower bound (inclusive) that the implementation will accept
     */
    default Long getLowerBound() {
        String lowerbound = config.getString("lowerbound");
        return StringUtils.isBlank(lowerbound) ? null : Long.valueOf(lowerbound);
    }

    /**
     * Reads upperbound from config file.
     * Can be overridden by implementations for different calculation strategies.
     * It is the implementations responsibility to make sure bounds are checked.
     * @return The upper bound (inclusive) that the implementation will accept
     */
    default Long getUpperBound() {
        String upperbound = config.getString("upperbound");
        return StringUtils.isBlank(upperbound) ? null : Long.valueOf(upperbound);
    }
}