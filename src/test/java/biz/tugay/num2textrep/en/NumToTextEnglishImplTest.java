package biz.tugay.num2textrep.en;

import biz.tugay.num2textrep.en.NumToTextEnglishImpl;
import org.junit.Assert;
import org.junit.Test;

public class NumToTextEnglishImplTest {

    private final NumToTextEnglishImpl numToTextEnglish = new NumToTextEnglishImpl();

    @Test
    public void mustReturnZero() {
        Assert.assertEquals("Zero", numToTextEnglish.wholeNumberToText("0"));
    }

    @Test
    public void mustNotReturnMinusZero() {
        Assert.assertEquals("Zero", numToTextEnglish.wholeNumberToText("-0"));
    }

    @Test
    public void mustReturnOne() {
        Assert.assertEquals("One", numToTextEnglish.wholeNumberToText("1"));
    }

    @Test
    public void mustReturnMinusOne() {
        Assert.assertEquals("Minus One", numToTextEnglish.wholeNumberToText("-1"));
    }

    @Test
    public void mustReturnTen() {
        Assert.assertEquals("Ten", numToTextEnglish.wholeNumberToText("10"));
    }

    @Test
    public void mustReturnFiveThousandTwoHundredAndThirtySeven() {
        Assert.assertEquals("Five thousand two hundred and thirty seven", numToTextEnglish.wholeNumberToText("5237"));
    }

    @Test
    public void mustReturnNineThousandAndFiftyFive() {
        Assert.assertEquals("Nine thousand and fifty five", numToTextEnglish.wholeNumberToText("9055"));

    }

    @Test
    public void mustReturnTenMillionAndOne() {
        Assert.assertEquals("Ten million and one", numToTextEnglish.wholeNumberToText("10000001"));
    }

    @Test
    public void mustReturnMinusTenMillion() {
        Assert.assertEquals("Minus Ten million and one", numToTextEnglish.wholeNumberToText("-10000001"));
    }

    @Test
    public void mustThrowIllegalArgumentExceptionForTooLarge() {
        try {
            numToTextEnglish.wholeNumberToText("1000000001");
            Assert.fail("No exception thrown! Exception should have been thrown since value is larger than upper bound declared in test config!");
        } catch (IllegalArgumentException ignored) {}
    }

    @Test
    public void mustThrowIllegalArgumentExceptionForTooSmallValue() {
        try {
            numToTextEnglish.wholeNumberToText("-1000000001");
            Assert.fail("No exception thrown! Exception should have been thrown since value is larger than upper bound declared in test config!");
        } catch (IllegalArgumentException ignored) {}
    }

}