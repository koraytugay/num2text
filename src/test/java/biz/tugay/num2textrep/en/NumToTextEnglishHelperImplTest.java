package biz.tugay.num2textrep.en;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class NumToTextEnglishHelperImplTest {

    NumToTextEnglishHelperImpl numToTextEnglishHelper = new NumToTextEnglishHelperImpl();

    @Test
    public void mustPartitionZero() {
        List<String> strings = numToTextEnglishHelper.partitionInThreeDigits("0");
        assertEquals(1, strings.size());
        assertEquals("0", strings.get(0));
    }

    @Test
    public void mustPartitionTen() {
        List<String> strings = numToTextEnglishHelper.partitionInThreeDigits("10");
        assertEquals(1, strings.size());
        assertEquals("10", strings.get(0));
    }

    @Test
    public void mustPartitionHundred() {
        List<String> strings = numToTextEnglishHelper.partitionInThreeDigits("100");
        assertEquals(1, strings.size());
        assertEquals("100", strings.get(0));
    }

    @Test
    public void mustPartitionThousand() {
        List<String> strings = numToTextEnglishHelper.partitionInThreeDigits("1000");
        assertEquals(2, strings.size());
        assertEquals("000", strings.get(0));
        assertEquals("1", strings.get(1));
    }

    @Test
    public void mustPartitionTenThousand() {
        List<String> strings = numToTextEnglishHelper.partitionInThreeDigits("10000");
        assertEquals(2, strings.size());
        assertEquals("000", strings.get(0));
        assertEquals("10", strings.get(1));
    }

    @Test
    public void mustPartitionHundredThousand() {
        List<String> strings = numToTextEnglishHelper.partitionInThreeDigits("100000");
        assertEquals(2, strings.size());
        assertEquals("000", strings.get(0));
        assertEquals("100", strings.get(1));
    }

    @Test
    public void mustPartitionMillion() {
        List<String> strings = numToTextEnglishHelper.partitionInThreeDigits("1000000");
        assertEquals(3, strings.size());
        assertEquals("000", strings.get(0));
        assertEquals("000", strings.get(1));
        assertEquals("1", strings.get(2));
    }

    // level 0 test
    @Test
    public void mustReturnZeroAtLevelZero() {
        Assert.assertEquals("zero", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("0", 0));
    }

    @Test
    public void mustReturnOneAtLevelZero() {
        Assert.assertEquals("one", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("1", 0));
    }

    @Test
    public void mustReturnTwoAtLevelZero() {
        Assert.assertEquals("two", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("2", 0));
    }

    @Test
    public void mustReturnThreeAtLevelZero() {
        Assert.assertEquals("three", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("3", 0));
    }

    @Test
    public void mustReturnTenAtLevelZero() {
        Assert.assertEquals("ten", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("10", 0));
    }

    @Test
    public void mustReturnElevenAtLevelZero() {
        Assert.assertEquals("eleven", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("11", 0));
    }

    @Test
    public void mustReturnFifteenAtLevelZero() {
        Assert.assertEquals("fifteen", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("15", 0));
    }

    @Test
    public void mustReturnTwentyAtLevelZero() {
        Assert.assertEquals("twenty", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("20", 0));
    }

    @Test
    public void mustReturnTwentyFiveAtLevelZero() {
        Assert.assertEquals("twenty five", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("25", 0));
    }

    @Test
    public void mustReturnOneHundredAtLevelZero() {
        Assert.assertEquals("one hundred", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("100", 0));
    }

    @Test
    public void mustReturnFiveHundredFiveAtLevelZero() {
        Assert.assertEquals("five hundred and five", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("505", 0));
    }

    @Test
    public void mustReturnFiveHundredFiftyFiveAtLevelZero() {
        Assert.assertEquals("five hundred and fifty five", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("555", 0));
    }

    @Test
    public void mustReturnOneThousandAtLevelOne() {
        Assert.assertEquals("one thousand", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("1", 1));
    }

    @Test
    public void mustReturnElevenThousandAtLevelOne() {
        Assert.assertEquals("eleven thousand", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("11", 1));
    }

    @Test
    public void mustReturnOneHundredThousandAtLevelOne() {
        Assert.assertEquals("one hundred thousand", numToTextEnglishHelper.resolveThreeDigitNumberConsideringLevel("100", 1));
    }

}