# NumToTextRep

- A toy project for converting numbers to text representations.
- A demo is running at: [https://koraytugay.com/numToText.md](https://koraytugay.com/numToText.md)
- Does not support numbers with decimal points
- Converts whole numbers to English only (No support for any other language)
- Supports up to `sextillion`

# Build & Execute
- Make sure you have [Apache Maven](https://maven.apache.org) and [Java 8 SDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- Build with `mvn install` in your local
  - You will have 2 jar files: `num2textrep-jar.jar` and `num2textrep-jar-with-dependencies`
  - `num2textrep-jar-with-dependencies` can be executed by `java -jar num2textrep-jar-with-dependencies`
  - You can also run the application using the maven exec plugin, see below
- You can run the application in interactive mode or batch mode, see below

## Running the Application in Interactive Mode
### Running the Application in Interactive Mode With jar File
- `java -jar num2textrep-jar-with-dependencies.jar`
- Follow the messages in the console

### Running the Application in Interactive Mode With Maven Exec Plugin
- `mvn exec:java`
- Follow the messages in the console

## Running the Application in Batch Mode
### Running the Application in Batch Mode With jar File
- `java -jar num2textrep-jar-with-dependencies.jar 1 2 3 4 1000 405`
  - Pass any number of arguments you want to be translated with a single space in between

### Running the Application in Batch Mode With Maven Exec Plugin
- `mvn exec:java -Dexec.args="1 2 3 4 1000 405"`
  - Pass any number of arguments you want to be translated within `-Dexec.args="1 2 3 4 1000 405"` as seen above 

# Sample Interactive Execution Using the Maven Exec Plugin
```
Korays-MacBook-Pro:num2text kt$ mvn exec:java
[INFO] Scanning for projects...
[INFO] 
[INFO] -----------------------< biz.tugay:num2textrep >------------------------
[INFO] Building num2textrep 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] >>> exec-maven-plugin:1.2.1:java (default-cli) > validate @ num2textrep >>>
[INFO] 
[INFO] <<< exec-maven-plugin:1.2.1:java (default-cli) < validate @ num2textrep <<<
[INFO] 
[INFO] 
[INFO] --- exec-maven-plugin:1.2.1:java (default-cli) @ num2textrep ---
Enter a whole number or 'q' to quit.
90
Ninety
Enter a whole number or 'q' to quit.
99
Ninety nine
Enter a whole number or 'q' to quit.
-10
Minus Ten
Enter a whole number or 'q' to quit.
12409213412
Twelve billion four hundred nine million two hundred thirteen thousand four hundred and twelve
Enter a whole number or 'q' to quit.
foobar
I can not process this input..
java.lang.NumberFormatException
	at biz.tugay.num2textrep.en.NumToTextEnglishImpl.wholeNumberToText(NumToTextEnglishImpl.java:27)
	at biz.tugay.num2textrep.App.main(App.java:20)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.codehaus.mojo.exec.ExecJavaMojo$1.run(ExecJavaMojo.java:297)
	at java.lang.Thread.run(Thread.java:748)
Enter a whole number or 'q' to quit.
4982189423189423189432189421942189421894891234823893481294231
Four ¯\_(ツ)_/¯ nine hundred eighty two ¯\_(ツ)_/¯ one hundred eighty nine ¯\_(ツ)_/¯ four hundred twenty three ¯\_(ツ)_/¯ one hundred eighty nine ¯\_(ツ)_/¯ four hundred twenty three ¯\_(ツ)_/¯ one hundred eighty nine ¯\_(ツ)_/¯ four hundred thirty two ¯\_(ツ)_/¯ one hundred eighty nine ¯\_(ツ)_/¯ four hundred twenty one ¯\_(ツ)_/¯ nine hundred forty two ¯\_(ツ)_/¯ one hundred eighty nine ¯\_(ツ)_/¯ four hundred twenty one ¯\_(ツ)_/¯ eight hundred ninety four sextillion eight hundred ninety one quintillion two hundred thirty four quadrillion eight hundred twenty three trillion eight hundred ninety three billion four hundred eighty one million two hundred ninety four thousand two hundred and thirty one
Enter a whole number or 'q' to quit.
q
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  22.076 s
[INFO] Finished at: 2019-03-30T16:15:43-04:00
[INFO] ------------------------------------------------------------------------
```

# Configuration
## Configuring Acceptable Range
- Acceptable value range can be configured modifying the `config.properties` file
  - The jar file can be unzipped and the file can be modified with a text editor
    - In that case `java -jar` will not work and the execution command will be `java biz.tugay.num2textrep.App` within the unzipped folder
  - `config.properties` in `target/classes` can be modified in case `mvn exec:java` is used
## Suppressing Stack Traces
### Suppressing Stack Traces With jar File
- `java -jar -DsupressStackTraces=true num2textrep-jar-with-dependencies.jar`
### Suppressing Stack Traces Using Maven Exec Plugin
- `mvn -DsupressStackTraces=true exec:java`
### Sample Execution Stack Traces Suppressed
```text
java -jar -DsupressStackTraces=true num2textrep-jar-with-dependencies.jar
Excepted values range: [-2147483648,2147483647]
Enter a whole number or 'q' to quit.
foo
Illegal Argument!
```